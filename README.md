# WASSERMANN 2
### Opis
Druga wersja automatycznej podlewaczki do kwiatów doniczkowych. Posiada własny zbiornik na wodę o pojemności 0.5L zazwyczaj wystarczający na kilka akcji podlewania jednego kwiatu, dzięki czemu możliwe jest pozostanie rośliny bez opieki na kilka/kilkanaście dni.

Projekt płytki PCB wykonany został z użyciem programu KiCAD, natomiast do wytworzenia samej płytki wykorzystano metodę termotransferu w warunkach domowych.

Serce urządzenia stanowi 8-bitowy mikrokontroler firmy Atmel zaprogramowany w języku C. 
### Główne funkcje  
* Dwa tryby podlewania: automatyczny i ręczny
* Podlewanie w trybie automatycznym może byc wyzwalane czasowo lub na podstawie wilgotności gleby
* Wskaźnik poziomu cieczy w zbiorniku
* Czujnik wilgotności gleby
* Czujnik wilgotności powietrza i temperatury
* Wyświetlacz

### Zdjęcie 
![Zdjęcie 1](/img/calosc.jpg "Widok całości")
![Zdjęcie 2](/img/plytka.jpg "Płytka elektroniczna")

### Błędy projektowe 
* Rezystancyjny pomiar wilgotności gleby (zamiast np. pojemnościowego) okazał sie niestabilny i niedokładny
* Brak potencjometru (sztywny dzielnik napięcia przy czujniku wilgotności gleby)
* Względnie duże zużycie energii z powodu braku możliwości wyłączenia niepotrzebych elementów (np. czujnika wilgotności powietrza)
* Brak możliwości wyłączenia napięcia na czujniku poziomu cieczy (elektroliza skutkiem ubocznym)
* Wyjście do programatora jest od strony ścieżek (utrudniony dostęp)
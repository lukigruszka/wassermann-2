#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/sleep.h>
#include "io_cfg.h"
#include "HD44780.h"

/*
 * Nigdy nie należy mieć włączonego switcha od wyświetlacza, gdy nie jest on zamontowany!
 */

struct {
	char moistureValue[4]; 
	char moisturePercent[4];
	char tempValue[3];
	char lightIntensity[4];
	char humidityValue[4]; //powietrze
}Sensors = {"256", "101", "21", "101", "101"};

void init(void);
void DisplayPage(short int);
void IdleMode(void);
void Watering(void);
void ResetTimer(void);

char freq[3];
char hoursLeft[5];
uint16_t maxHours = 96, hours = 0;
uint8_t counter = 0;
uint8_t seconds = 0, minutes = 0;
uint8_t LCDWasTurnedOff = 1;

int main(void){
	uint8_t pageNumber = 0; 
	const uint8_t nOfPages = 3;

	init();
	//LCD_Initalize();
	
	while(1){
		//LCD_RW_PORT &= ~LCD_RW;
		if(LCDWasTurnedOff && displayState)
			LCD_Initalize();
		
		if(hours >= maxHours)
			Watering();
		
		if(displayState){
			LCDWasTurnedOff = 0;
			if(!(lButton) || !(rButton)){
				if(pageNumber != HDIPage){
					DisplayPage(pageNumber = HDIPage);
					while(!(lButton) || !(rButton));
					_delay_ms(10);
				}
				counter = 0;
				if(!(rButton) && maxHours < 99*24)
					maxHours+=24;
				else if(!(lButton) && maxHours > 24)
					maxHours-=24;
				DisplayPage(pageNumber = HDIPage);
				while(!(lButton) || !(rButton));
				_delay_ms(10);
			}
			else if(counter >= pageFreq){
				ADCSRA |= (1<<ADSC); //Pomiar wilgotnosci
				counter = 0;
				while(ADCSRA & (1<<ADSC));
				DisplayPage(pageNumber = ++pageNumber%nOfPages);
			}
		}
		else{
			IdleMode();
			LCDWasTurnedOff = 1;
		}
	}
}

void DisplayPage(short int pageNumber){
	switch(pageNumber){
		case 0:
			snprintf(Sensors.moistureValue, 5, "%d", ADCH);
			snprintf(Sensors.moisturePercent, 4, "%d", 100*(moistureMin-ADCH)/(moistureMin-moistureMax));
			LCD_Clear();
			LCD_GoTo(0, 0);
			LCD_WriteText("Wil. gleby: ");
			LCD_WriteText(Sensors.moistureValue);
			//LCD_WriteText(moisturePercent);
			LCD_WriteText("%");
			LCD_GoTo(0, 1);
			LCD_WriteText("Temperatura: ");
			LCD_WriteText(Sensors.tempValue);
			break;
		case 1:
			LCD_Clear();
			LCD_GoTo(0, 0);
			LCD_WriteText("Wil. pow.:  ");
			LCD_WriteText(Sensors.humidityValue);
			LCD_WriteText("%");
			LCD_GoTo(0, 1);
			LCD_WriteText("Swiatlo:    ");
			LCD_WriteText(Sensors.lightIntensity);
			LCD_WriteText("%");
			break;
		case 2:
			snprintf(freq, 3, "%d", maxHours/24);
			snprintf(hoursLeft, 5, "%d", maxHours-hours);
			LCD_Clear();
			LCD_GoTo(0, 0);
			LCD_WriteText("Akcja co: ");
			LCD_WriteText(freq);
			LCD_WriteText(" dni");
			LCD_GoTo(0, 1);
			LCD_WriteText("Pozostalo: ");
			LCD_WriteText(hoursLeft);
			LCD_WriteText("h");
			break;	
	}
}

void init(void){
	redDiodeInit;
	blueDiodeInit;
	buzzerInit;
	pumpInit;
	pumpStateInit;
	moistureSensorInit;
	displayInit;
	lButtonInit;
	rButtonInit;
	
	blueDiodeOff; //Jakaś akcja, np. pompowanie
	redDiodeOn;	//Stan włączony i nie uśpiony
	
	/* ADC */
	ADCSRA = (1<<ADPS0) | (1<<ADPS1) //prescaler 8 --> 130kHz
		| (1<<ADEN) | (1<<ADSC);
	ADMUX = (1<< REFS1) | (1<<REFS0) //Internal 2.56V Voltage Reference
		| (1<<ADLAR)				 //dosuwanie do lewej (ADCH ma 8 starszych bitów)
		| (1<<MUX2);
		
	/* Interrupts 4Hz */
	TCCR0 |= (1<<CS02) | (1<<CS00); // źródłem CLK, preskaler 1024  
	TIMSK |= (1<<TOIE0); 			//przepełnienie
	
	/* Sleep mode */
	set_sleep_mode(SLEEP_MODE_IDLE);
	
	sei(); //włączenie opcji przerwań
}

void IdleMode(void){
	redDiodeOff;
	while(!(displayState) && hours < maxHours)
		sleep_enable();
	redDiodeOn;
}

void Watering(void){
	//blueDiodeOn;
	ResetTimer();
	
	//podlewanie
	pumpOn;
	_delay_ms(6000);
	pumpOff;
	//koniec podlewania
	
	blueDiodeOff;
}

void ResetTimer(void){
	seconds = 0;
	minutes = 0;
	hours = 0;
}

ISR(TIMER0_OVF_vect){
	counter++;
	seconds++;
	minutes += seconds/(4*60);
	hours += minutes/60;
	seconds %= (4*60);
	minutes %= 60;
}

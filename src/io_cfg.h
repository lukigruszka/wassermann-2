#ifndef _io_cfg_h_
#define _io_cfg_h_

#define F_CPU 			1000000L

#define moistureMin		255
#define moistureMax		200

#define HDIPage			2
#define pageFreq		12 //godzin

#define redDiodeInit	DDRD  |= (1<<PD4)
#define redDiodeOn 		PORTD &= ~(1<<PD4)
#define redDiodeOff 	PORTD |= (1<<PD4)
#define redDiodeSwitch	PORTD ^= (1<<PD4)

#define blueDiodeInit 	DDRD  |= (1<<PD3)
#define blueDiodeOn 	PORTD &= ~(1<<PD3)
#define blueDiodeOff 	PORTD |= (1<<PD3)
#define blueDiodeSwitch	PORTD ^= (1<<PD3)

#define buzzerInit 		DDRB  |= (1<<PB6)
#define buzzerOn 		PORTB |= (1<<PB6)
#define buzzerOff 		PORTB &= ~(1<<PB6)
#define buzzerSwitch	PORTB ^= (1<<PB6)

#define pumpInit		DDRC  |= (1<<PC3)
#define pumpOff			PORTC &= ~(1<<PC3)
#define pumpOn			PORTC |= (1<<PC3)

#define pumpStateInit	DDRC &= ~(1<<PC2); PORTC |= (1<<PC2)
#define pumpState		PINC & 	(1<<PC2)

#define lButtonInit		DDRD &= ~(1<<PD1); PORTD |= (1<<PD1)
#define lButton 		PIND &	(1<<PD1)
#define rButtonInit		DDRD &= ~(1>>PD0); PORTD |= (1<<PD0)
#define rButton 		PIND &	(1<<PD0)

#define displayInit		DDRC &= ~(1<<PC0); PORTC &= ~(1<<PC0)
#define	displayState	PINC & (1<<PC0)

#define moistureSensorInit DDRC &= ~(1<<PC4)


#endif
